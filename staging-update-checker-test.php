<?php
/**
* Plugin Name: Staging Update Checker Test
* Plugin URI: https://www.yourwebsiteurl.com/
* Description: This is the plugin for test purposes.
* Version: 1.1
* Author: Stoimenovski
* Author URI: http://yourwebsiteurl.com/
**/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/stoimenovski/staging-update-checker-test/',
	__FILE__,
	'staging-update-checker-test'
);

$myUpdateChecker->setBranch('master');

function test(){
    echo 12332211299999999999999999999;
}

function update_test(){
	echo "updaded";
}

function patch_test(){
	echo "testing git patch";
}
